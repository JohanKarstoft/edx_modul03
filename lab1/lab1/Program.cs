using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            int result = sum(20, 10);
            Console.WriteLine($"the sum of 20 and 10 is {result}");

            int result3 = sum(20, 10, 5);
            Console.WriteLine($"The sum of 20, 10 and 5 is {result3}");

            double resultdbl = sum(20.5, 10.5);
            Console.WriteLine($"The sum of 20.5 and 10.5 is {resultdbl}");

            Console.ReadKey();

        }
        // method that returns no value
        static int sum(int first, int second)
        {
            int sum = first + second;
            return sum;
        }
        static int sum(int first, int second, int third)
        {
            int sum = first + second + third;
            return sum;
        }
        static double sum(double first, double second)
        {
            double sum = first + second;
            return sum;
        }
    }
}
