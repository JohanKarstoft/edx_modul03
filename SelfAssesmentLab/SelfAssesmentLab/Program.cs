using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfAssesmentLab
{
    class Program
    {
        static void Main(string[] args)
        {

            GetStudentInformation();

            Console.ReadKey();
            
        }

        static void GetStudentInformation ()
        {
            Console.WriteLine("Enter Student's first name");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter student's Last name");
            string lastName = Console.ReadLine();
            Console.WriteLine("Enter date of Birth");
            DateTime dateOfBirth = DateTime.Parse(Console.ReadLine());

            PrintStudentDetails(firstName, lastName, dateOfBirth);
        }

        static void PrintStudentDetails(string firstName, string lastName, DateTime dateOfBirth)
        {
            Console.WriteLine("{0} {1} was born on {2}", firstName, lastName, dateOfBirth);
        }
    }
}
