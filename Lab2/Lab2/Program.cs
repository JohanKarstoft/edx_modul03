using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            int first = 9, second = 0;
            Console.WriteLine("Enter the number to be divided");
            first = System.Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter the number to divide with");
            second = System.Int32.Parse(Console.ReadLine());
            
            int result = Divide(first, second);
            Console.WriteLine("{0} divided by {1} is {2}", first, second, result);
            Console.ReadKey();
        }

        static int Divide(int first, int second)
        {
            int result = 0;
            try
            {
                result = first / second;
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Cannot divide by zero, please use a non-zero value");
            }
            
            return result;
        }
    }
}
